﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using RenovadorAthena.Resources;
using System.IO.IsolatedStorage;

namespace RenovadorAthena.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            this.Items = new ObservableCollection<ItemViewModel>();
        }

        /// <summary>
        /// A collection for ItemViewModel objects.
        /// </summary>
        public ObservableCollection<ItemViewModel> Items 
        { 
            get; 
            private set; 
        }

        private string _sampleProperty = "Sample Runtime Property Value";
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding
        /// </summary>
        /// <returns></returns>
        public string SampleProperty
        {
            get
            {
                return _sampleProperty;
            }
            set
            {
                if (value != _sampleProperty)
                {
                    _sampleProperty = value;
                    NotifyPropertyChanged("SampleProperty");
                }
            }
        }

        /// <summary>
        /// Sample property that returns a localized string
        /// </summary>
        public string LocalizedSampleProperty
        {
            get
            {
                return AppResources.SampleProperty;
            }
        }

        public bool IsDataLoaded
        {
            get;
            set;
        }

        private AsyncCallback callbackPage;

        /// <summary>
        /// Creates and adds a few ItemViewModel objects into the Items collection.
        /// </summary>
        public void LoadData(AsyncCallback callback)
        {
            callbackPage = callback;
            App.DataConexao.RetornarLivros(RetLivrosCallback);
            // Sample data; replace with real data
            //this.Items.Add(new ItemViewModel() { ID = "0", Titulo = "runtime one", Status = "Maecenas praesent accumsan bibendum", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "1", Titulo = "runtime two", Status = "Dictumst eleifend facilisi faucibus", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "2", Titulo = "runtime three", Status = "Habitant inceptos interdum lobortis", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "3", Titulo = "runtime four", Status = "Nascetur pharetra placerat pulvinar", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "4", Titulo = "runtime five", Status = "Maecenas praesent accumsan bibendum", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "5", Titulo = "runtime six", Status = "Dictumst eleifend facilisi faucibus", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "6", Titulo = "runtime seven", Status = "Habitant inceptos interdum lobortis", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "7", Titulo = "runtime eight", Status = "Nascetur pharetra placerat pulvinar", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "8", Titulo = "runtime nine", Status = "Maecenas praesent accumsan bibendum", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "9", Titulo = "runtime ten", Status = "Dictumst eleifend facilisi faucibus", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "10", Titulo = "runtime eleven", Status = "Habitant inceptos interdum lobortis", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "11", Titulo = "runtime twelve", Status = "Nascetur pharetra placerat pulvinar", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "12", Titulo = "runtime thirteen", Status = "Maecenas praesent accumsan bibendum", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "13", Titulo = "runtime fourteen", Status = "Dictumst eleifend facilisi faucibus", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "14", Titulo = "runtime fifteen", Status = "Habitant inceptos interdum lobortis", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
            //this.Items.Add(new ItemViewModel() { ID = "15", Titulo = "runtime sixteen", Status = "Nascetur pharetra placerat pulvinar", DataDevolucao = Convert.ToDateTime("01/01/2012", new System.Globalization.CultureInfo("pt-BR")) });
        }

        public void LoadBooks()
        {
            if (IsolatedStorageSettings.ApplicationSettings.Contains(App.BooksID))
            {
                App.ViewModel.Items = IsolatedStorageSettings.ApplicationSettings[App.BooksID] as ObservableCollection<ItemViewModel>;
            }
            IsDataLoaded = true;
        }

        public void RetLivrosCallback(IAsyncResult res)
        {
            this.IsDataLoaded = true;
            callbackPage(res);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}