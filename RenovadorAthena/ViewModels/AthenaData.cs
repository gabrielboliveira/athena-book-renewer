﻿using HtmlAgilityPack;
using RenovadorAthena.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace RenovadorAthena
{
    public class AthenaData
    {
        private string siteAthenaBase = "https://www.athena.biblioteca.unesp.br/F/";
        public string SiteAthenaBase
        {
            get { return siteAthenaBase; }
        }
        private string siteAthena = "http://www.athena.biblioteca.unesp.br/F/?func=BOR-INFO";

        public string SiteAthena
        {
            get { return siteAthena; }
        }
        
        private string siteAthenaLogin = "http://www.athena.biblioteca.unesp.br/F/ID_TOKEN?func=BOR-INFO&ssl_flag=Y&func=login-session&login_source=LOGIN-BOR&bor_id=ID_USUARIO&bor_verification=ID_SENHA&bor_library=UEP50";

        public string SiteAthenaLogin
        {
            get 
            {
                return siteAthenaLogin.Replace("ID_USUARIO", usuario).Replace("ID_SENHA", senha).Replace("ID_TOKEN", token);
            }
        }

        private string siteAthenaRenovar = "http://www.athena.biblioteca.unesp.br/F/ID_TOKEN?func=bor-renew-all&adm_library=UEP50";

        public string SiteAthenaRenovar
        {
            get 
            {
                return siteAthenaRenovar.Replace("ID_TOKEN", token); 
            }
        }
        private string siteAthenaLivros = "http://www.athena.biblioteca.unesp.br/F/ID_TOKEN?func=bor-loan&adm_library=UEP50";

        public string SiteAthenaLivros
        {
            get 
            { 
                return siteAthenaLivros.Replace("ID_TOKEN", token); 
            }
        }

        private string usuario;

        public string Usuario
        {
            get { return usuario; }
            set { usuario = value; }
        }

        private string senha;

        public string Senha
        {
            get { return senha; }
            set { senha = value; }
        }

        private string token;

        private string Token
        {
            get { return token; }
            set { token = value; }
        }

        private bool senhaIncorreta = false;

        public bool SenhaIncorreta
        {
            get { return senhaIncorreta; }
            set { senhaIncorreta = value; }
        }

        private bool erro = false;

        public bool Erro
        {
            get { return erro; }
            set { erro = value; }
        }

        private bool usuarioLogado = false;

        public bool UsuarioLogado
        {
            get { return usuarioLogado; }
            set { usuarioLogado = value; }
        }

        private bool finalizado = false;

        public bool Finalizado
        {
            get { return finalizado; }
            set { finalizado = value; }
        }

        private Dispatcher activeDispatcher;

        public Dispatcher ActiveDispatcher
        {
            get { return activeDispatcher; }
            set { activeDispatcher = value; }
        }

        private DateTime horaLogin = DateTime.Now;

        private HttpWebRequest webRequest;
        private AsyncCallback callbackLogin, callbackLivros, callbackRenovar;


        #region Login

        public void LogarUsuario(AsyncCallback callback)
        {
            try
            {
                // Verifica se a diferença entre o login efetuado e quando for usado é de 15 minutos
                if (((DateTime.Now.Minute - horaLogin.Minute) < 15) && !erro && !senhaIncorreta && usuarioLogado)
                {
                    callback(null);
                }
                else
                {
                    callbackLogin = callback;
                    webRequest = (HttpWebRequest)HttpWebRequest.Create(SiteAthena);
                    webRequest.Headers["Connection"] = "Keep-Alive";
                    webRequest.BeginGetResponse(LogarUsuarioCallback, webRequest);
                }
            }
            catch (Exception e)
            {
                erro = true;
                callback(null);
            }
        }

        private void LogarUsuarioCallback(IAsyncResult resultado)
        {
            try
            {
                webRequest = resultado.AsyncState as HttpWebRequest;
                if (webRequest != null)
                {
                    HttpWebResponse response = (HttpWebResponse)webRequest.EndGetResponse(resultado);
                    Stream streamResponse = response.GetResponseStream();
                    StreamReader streamRead = new StreamReader(streamResponse);
                    string responseString = streamRead.ReadToEnd();
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(responseString);
                    HtmlNode link = doc.DocumentNode.DescendantNodes().Where(n => n.Name == "form").FirstOrDefault();
                    string tokenLogin = "";
                    if (link != null)
                    {
                        tokenLogin = link.Attributes["action"].Value;
                        tokenLogin = tokenLogin.Remove(0, tokenLogin.IndexOf("/F/") + 3);
                        Token = tokenLogin;

                        webRequest = (HttpWebRequest)HttpWebRequest.Create(SiteAthenaLogin);
                        webRequest.Headers["Connection"] = "Keep-Alive";
                        webRequest.BeginGetResponse(LoginExecutadoCallback, webRequest);
                    }
                    else
                    {
                        erro = true;
                        callbackLogin(null);
                    }
                }
                else
                {
                    erro = true;
                    callbackLogin(null);
                }
            }
            catch (WebException e)
            {
                erro = true;
                callbackLogin(null);
            }
        }

        private void LoginExecutadoCallback(IAsyncResult resultado)
        {
            try
            {
                webRequest = resultado.AsyncState as HttpWebRequest;
                if (webRequest != null)
                {
                    HttpWebResponse response = (HttpWebResponse)webRequest.EndGetResponse(resultado);
                    Stream streamResponse = response.GetResponseStream();
                    StreamReader streamRead = new StreamReader(streamResponse);
                    string responseString = streamRead.ReadToEnd();
                    if (responseString.Contains("<!-- filename: login-session-uep01 -->"))
                    {
                        erro = true;
                        senhaIncorreta = true;
                        callbackLogin(null);
                    }
                    else
                    {
                        erro = false;
                        senhaIncorreta = false;
                        usuarioLogado = true;
                        horaLogin = DateTime.Now;
                        callbackLogin(resultado);
                    }
                }
                else
                {
                    erro = true;
                    callbackLogin(null);
                }
            }
            catch (Exception e)
            {
                erro = true;
                callbackLogin(null);
            }
        }
        #endregion

        #region Carregar Livros
        public void RetornarLivros(AsyncCallback callback)
        {
            callbackLivros = callback;
            LogarUsuario(RetornarLivrosLoginCallback);
        }

        private void RetornarLivrosLoginCallback(IAsyncResult resultado)
        {
            if (!erro && !senhaIncorreta && usuarioLogado)
            {
                webRequest = (HttpWebRequest)HttpWebRequest.Create(SiteAthenaLivros);
                webRequest.Headers["Connection"] = "Keep-Alive";
                webRequest.BeginGetResponse(RetornarLivrosCarregarCallback, webRequest);
            }
            else
            {
                callbackLivros(null);
            }
        }

        private void RetornarLivrosCarregarCallback(IAsyncResult resultado)
        {
            webRequest = resultado.AsyncState as HttpWebRequest;
            if (webRequest != null)
            {
                HttpWebResponse response = (HttpWebResponse)webRequest.EndGetResponse(resultado);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();

                if (!responseString.Contains("<!-- filename: bor-loan-no-loan-->"))
                {
                    activeDispatcher.BeginInvoke(() =>
                    {
                        App.ViewModel.Items.Clear();
                    });
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(responseString);
                    //List<HtmlNode> node = doc.DocumentNode.DescendantNodes().Where(n => n.Name == "table").ToList();
                    HtmlNode table = doc.DocumentNode.DescendantNodes().Where(n => n.Name == "table").ToList()[5];
                    int idCount = 0;
                    int count = 0;
                    foreach (HtmlNode tr in table.ChildNodes.Where(n => n.Name == "tr"))
                    {
                        if (count == 0)
                        {
                            count++;
                            continue;
                        }
                        //if (tr.Attributes["class"] != null && tr.Attributes["class"].Value.Equals("tr1"))
                        else
                        {
                            activeDispatcher.BeginInvoke(() =>
                            {
                                App.ViewModel.Items.Add(new ItemViewModel()
                                {
                                    ID = idCount++.ToString(),
                                    Titulo = tr.ChildNodes.Where(n => n.Name == "td").ToList()[3].InnerText,
                                    StringDevolucao = tr.ChildNodes.Where(n => n.Name == "td").ToList()[5].InnerText.Substring(0,8)
                                });
                            });
                        } 
                    }
                    activeDispatcher.BeginInvoke(() =>
                    {
                        callbackLivros(resultado);
                    });
                }
                else
                {
                    callbackLivros(resultado);
                }
                
            }
            else
            {
                erro = true;
                callbackLogin(null);
            }
        }
        #endregion

        public void RenovarLivros(AsyncCallback callback)
        {
            callbackRenovar = callback;
            LogarUsuario(RenovarLivrosLoginCallback);
        }

        private void RenovarLivrosLoginCallback(IAsyncResult res)
        {
            if (!erro && !senhaIncorreta && usuarioLogado)
            {
                webRequest = (HttpWebRequest)HttpWebRequest.Create(SiteAthenaRenovar);
                webRequest.Headers["Connection"] = "Keep-Alive";
                webRequest.BeginGetResponse(RenovarLivrosCarregarCallback, webRequest);
            }
            else
            {
                erro = true;
                senhaIncorreta = true;
                usuarioLogado = false;
                callbackLogin(null);
            }
        }

        private void RenovarLivrosCarregarCallback(IAsyncResult resultado)
        {
            webRequest = resultado.AsyncState as HttpWebRequest;
            if (webRequest != null)
            {
                HttpWebResponse response = (HttpWebResponse)webRequest.EndGetResponse(resultado);
                Stream streamResponse = response.GetResponseStream();
                StreamReader streamRead = new StreamReader(streamResponse);
                string responseString = streamRead.ReadToEnd();

                if (responseString.Contains("<!--filename: bor-renew-all-body-->"))
                {
                    activeDispatcher.BeginInvoke(() =>
                    {
                        App.ViewModel.Items.Clear();
                    });
                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(responseString);
                    List<HtmlNode> node = doc.DocumentNode.DescendantNodes().Where(n => n.Name == "table").ToList();
                    HtmlNode table = doc.DocumentNode.DescendantNodes().Where(n => n.Name == "table").ToList()[4];
                    int idCount = 0;
                    int count = 0;
                    foreach (HtmlNode tr in table.ChildNodes.Where(n => n.Name == "tr"))
                    {
                        if (count == 0)
                        {
                            count++;
                            continue;
                        }
                        //if (tr.Attributes["class"] != null && tr.Attributes["class"].Value.Equals("tr1"))
                        else
                        {
                            activeDispatcher.BeginInvoke(() =>
                            {
                                string strDevolucao = tr.ChildNodes.Where(n => n.Name == "td").ToList()[3].InnerText;
                                bool reservado = strDevolucao.Length > 8;
                                App.ViewModel.Items.Add(new ItemViewModel()
                                {
                                    ID = idCount++.ToString(),
                                    Reservado = reservado,
                                    Titulo = tr.ChildNodes.Where(n => n.Name == "td").ToList()[1].InnerText,
                                    StringDevolucao = tr.ChildNodes.Where(n => n.Name == "td").ToList()[3].InnerText.Substring(0, 8)
                                });
                            });
                        }
                    }
                    activeDispatcher.BeginInvoke(() =>
                    {
                        erro = false;
                        callbackRenovar(resultado);
                    });
                }
                else
                {
                    erro = true;
                    callbackRenovar(resultado);
                }
            }
            else
            {
                erro = true;
                callbackLogin(null);
            }
        }
    }
}
