﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;

namespace RenovadorAthena.ViewModels
{
    public class ItemViewModel : INotifyPropertyChanged
    {
        private string _id;
        /// <summary>
        /// Sample ViewModel property; this property is used to identify the object.
        /// </summary>
        /// <returns></returns>
        public string ID
        {
            get
            {
                return _id;
            }
            set
            {
                if (value != _id)
                {
                    _id = value;
                    NotifyPropertyChanged("ID");
                }
            }
        }

        private string _titulo;
        /// <summary>
        /// Titulo do livro
        /// </summary>
        /// <returns></returns>
        public string Titulo
        {
            get
            {
                return _titulo;
            }
            set
            {
                if (value != _titulo)
                {
                    _titulo = value;
                    NotifyPropertyChanged("Titulo");
                }
            }
        }

        private string _status;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public string Status
        {
            get
            {
                return _status;
            }
            set
            {
                if (value != _status)
                {
                    _status = value;
                    NotifyPropertyChanged("Status");
                }
            }
        }

        private DateTime _dataDevolucao;
        /// <summary>
        /// Sample ViewModel property; this property is used in the view to display its value using a Binding.
        /// </summary>
        /// <returns></returns>
        public DateTime DataDevolucao
        {
            get
            {
                return _dataDevolucao;
            }
            set
            {
                if (value != _dataDevolucao)
                {
                    _dataDevolucao = value;
                    _stringDevolucao = _dataDevolucao.Day.ToString("00") + "/" + _dataDevolucao.Month.ToString("00") + "/" + _dataDevolucao.Year.ToString("00");
                    NotifyPropertyChanged("DataDevolucao");
                    CheckStatus();
                }
            }
        }

        private string _stringDevolucao;

        public string StringDevolucao
        {
            get { return _stringDevolucao; }
            set 
            { 
                _stringDevolucao = value;
                _dataDevolucao = new DateTime(2000 + Convert.ToInt32(_stringDevolucao.Substring(6)), Convert.ToInt32(_stringDevolucao.Substring(3, 2)), Convert.ToInt32(_stringDevolucao.Substring(0, 2)));
                CheckStatus();
                NotifyPropertyChanged("StringDevolucao");
            }
        }

        private bool reservado = false;

        public bool Reservado
        {
            get { return reservado; }
            set { reservado = value; }
        }

        private void CheckStatus()
        {
            int totalDias = (_dataDevolucao - DateTime.Now).Days;
            if( totalDias >= 7 )
            {
                Status = "Renovado";
            }
            else if ( totalDias < 7 && totalDias > 0)
            {
                Status = "Devolver em " + totalDias + " dias";
            }
            else if (totalDias < 0)
            {
                Status = "Vencido!";
            }
            else if (totalDias == 0)
            {
                Status = "Devolver hoje!";
            }
            if(reservado)
                Status = "Reservado!!!";
        }

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(String propertyName)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (null != handler)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}