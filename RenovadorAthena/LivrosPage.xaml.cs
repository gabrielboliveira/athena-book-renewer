﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using RenovadorAthena.Resources;
using RenovadorAthena.ViewModels;
using System.Windows.Media;

namespace RenovadorAthena
{
    public partial class MainPage : PhoneApplicationPage
    {
        // Constructor
        public MainPage()
        {
            InitializeComponent();

            // Set the data context of the LongListSelector control to the sample data
            DataContext = App.ViewModel;
            App.DataConexao.ActiveDispatcher = Dispatcher;

            // Sample code to localize the ApplicationBar
            BuildLocalizedApplicationBar();
        }

        // Load data for the ViewModel Items
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (!App.ViewModel.IsDataLoaded)
            {
                if (App.NetworkAvailable) 
                {
                    App.ProgressIndicator(true);
                    App.ViewModel.LoadData(CallbackLoadData);
                }
                else
                {
                    App.ViewModel.LoadBooks();
                }
            }

            NavigationService.RemoveBackEntry();
        }

        private void CallbackLoadData(IAsyncResult res)
        {
            Dispatcher.BeginInvoke(() =>
            {
                App.ProgressIndicator(false);
            });
            if (App.DataConexao.Erro)
            {
                if (App.DataConexao.SenhaIncorreta)
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show(AppResources.LoginErrorText, AppResources.LoginErrorTitle, MessageBoxButton.OK);
                    });
                }
                else
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show(AppResources.ConnectionErrorText, AppResources.ConnectionErrorTitle, MessageBoxButton.OK);
                    });
                }
            }
            else
            {
                if (App.ViewModel.Items.Count == 0)
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        MainLongListSelector.Visibility = System.Windows.Visibility.Collapsed;
                        SemLivrosTextBlock.Visibility = System.Windows.Visibility.Visible;
                    });
                }
                else
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        MainLongListSelector.Visibility = System.Windows.Visibility.Visible;
                        SemLivrosTextBlock.Visibility = System.Windows.Visibility.Collapsed;
                    });
                }
            }
        }

        // Sample code for building a localized ApplicationBar
        private void BuildLocalizedApplicationBar()
        {
            // Set the page's ApplicationBar to a new instance of ApplicationBar.
            ApplicationBar = new ApplicationBar();

            // Create a new button and set the text value to the localized string from AppResources.
            ApplicationBarIconButton appBarRenewButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/download.png", UriKind.Relative));
            appBarRenewButton.Text = AppResources.AppBarRenewButtonText;
            appBarRenewButton.Click += appBarRenewButton_Click;
            ApplicationBar.Buttons.Add(appBarRenewButton);

            // Create a new button and set the text value to the localized string from AppResources.
            ApplicationBarIconButton appBarRefreshButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/refresh.png", UriKind.Relative));
            appBarRefreshButton.Text = AppResources.AppBarRefreshButtonText;
            appBarRefreshButton.Click += appBarRefreshButton_Click;
            ApplicationBar.Buttons.Add(appBarRefreshButton);

            ApplicationBarMenuItem appBarLogoutMenuItem = new ApplicationBarMenuItem(AppResources.AppBarLogoutMenuItemText);
            appBarLogoutMenuItem.Click += appBarLogoutMenuItem_Click;
            ApplicationBar.MenuItems.Add(appBarLogoutMenuItem);

            // Create a new menu item with the localized string from AppResources.
            ApplicationBarMenuItem appBarAboutMenuItem = new ApplicationBarMenuItem(AppResources.AppBarAboutMenuItemText);
            appBarAboutMenuItem.Click += appBarAboutMenuItem_Click;
            ApplicationBar.MenuItems.Add(appBarAboutMenuItem);
        }

        void appBarAboutMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        void appBarLogoutMenuItem_Click(object sender, EventArgs e)
        {
            App.Logout();
            NavigationService.Navigate(new Uri("/LoginPage.xaml", UriKind.Relative));
        }

        private void appBarRefreshButton_Click(object sender, EventArgs e)
        {
            if (App.NetworkAvailable)
            {
                App.ProgressIndicator(true);
                App.DataConexao.RetornarLivros(CallbackLoadData);
            }
            else
            {
                App.ProgressIndicator(false);
                MessageBox.Show(AppResources.NetworkNotAvailableText, AppResources.NetworkNotAvailableTitle, MessageBoxButton.OK);
            }
        }

        private void appBarRenewButton_Click(object sender, EventArgs e)
        {
            if (App.ViewModel.Items.Count() > 0)
            {
                if (App.NetworkAvailable)
                {
                    App.ProgressIndicator(true);
                    App.DataConexao.RenovarLivros(RenLivrosCallback);
                }
                else
                {
                    MessageBox.Show(AppResources.NetworkNotAvailableText, AppResources.NetworkNotAvailableTitle, MessageBoxButton.OK);
                    App.ProgressIndicator(false);
                }
            }
            else
            {
                MessageBox.Show(AppResources.NoBooksRenewText, AppResources.NoBooksRenewTitle, MessageBoxButton.OK);
            }
        }

        private void RenLivrosCallback(IAsyncResult ar)
        {
            Dispatcher.BeginInvoke(() =>
            {
                App.ProgressIndicator(false);
            });
            if (App.DataConexao.Erro)
            {
                if (App.DataConexao.SenhaIncorreta || !App.DataConexao.UsuarioLogado)
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show(AppResources.LoginErrorText, AppResources.LoginErrorTitle, MessageBoxButton.OK);
                        App.Logout();
                        NavigationService.Navigate(new Uri("/LoginPage.xaml", UriKind.Relative));
                    });
                }
            }
            else
            {
                Dispatcher.BeginInvoke(() =>
                {
                    bool reservado = false;
                    foreach (ItemViewModel item in App.ViewModel.Items)
                    {
                        if (item.Reservado)
                        {
                            reservado = true;
                            break;
                        }
                    }
                    if (reservado)
                    {
                        MessageBox.Show(AppResources.BookReservedText, AppResources.BookReservedTitle, MessageBoxButton.OK);
                    }
                    else
                    {
                        MessageBox.Show(AppResources.RenewSuccessText, AppResources.RenewSuccessTitle, MessageBoxButton.OK);
                    }
                });
                
            }
        }
    }
}