using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using RenovadorAthena.Resources;
using RenovadorAthena.ViewModels;
using System.Windows.Input;
using System.Text.RegularExpressions;

namespace RenovadorAthena
{
    public partial class LoginPage : PhoneApplicationPage
    {

        // Constructor
        public LoginPage()
        {
            InitializeComponent();

            // Sample code to localize the ApplicationBar
            BuildLocalizedApplicationBar();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (App.Usuario != null && App.Senha != null)
            {
                App.DataConexao.Usuario = App.Usuario;
                App.DataConexao.Senha = App.Senha;
                //App.LoadBooks();
                NavigationService.Navigate(new Uri("/LivrosPage.xaml", UriKind.Relative));
            }

            if (NavigationService.BackStack.Count() > 0)
                NavigationService.RemoveBackEntry();
            base.OnNavigatedTo(e);
        }

        #region Validar texto inserido

        string _enteredPasscode = "";
        string _passwordChar = "*";
        string _actualPassword = "";
        int contCaracteresUser = 0;
        int contCaracteres = 0;
        bool bloqueia = false;

        private void UsuarioTextBox_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                    contCaracteresUser++;
                    break;
                case Key.Back:
                    if (contCaracteresUser > 0) contCaracteresUser--;
                    break;
                default:
                    e.Handled = true;
                    break;
            }
        }

        private void UsuarioTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if (contCaracteresUser == 11)
                SenhaTextBox.Focus();
        }

        private string GetNewPasscode(string oldPasscode, KeyEventArgs keyEventArgs)
        {
            string newPasscode = string.Empty;
            switch (keyEventArgs.Key)
            {
                case Key.D0:
                case Key.D1:
                case Key.D2:
                case Key.D3:
                case Key.D4:
                case Key.D5:
                case Key.D6:
                case Key.D7:
                case Key.D8:
                case Key.D9:
                    if (contCaracteres < 4) {
                        contCaracteres++;
                        newPasscode = oldPasscode + (keyEventArgs.PlatformKeyCode - 48);
                    }
                    else
                    {
                        newPasscode = oldPasscode;
                    }
                    break;
                case Key.Back:
                    if (oldPasscode.Length > 0) 
                    {
                        contCaracteres--;
                        newPasscode = oldPasscode.Substring(0, oldPasscode.Length - 1);
                    }
                    break;
                default:
                    //others
                    newPasscode = oldPasscode;
                    break;
            }
            bloqueia = contCaracteres == 4;
            return newPasscode;
        }

        private void SenhaTextBox_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            //modify new passcode according to entered key
            _enteredPasscode = GetNewPasscode(_enteredPasscode, e);
            _actualPassword = _enteredPasscode;
            //replace text by *
            SenhaTextBox.Text = Regex.Replace(_enteredPasscode, @".", _passwordChar);

            //take cursor to end of string
            SenhaTextBox.SelectionStart = SenhaTextBox.Text.Length;
            if (bloqueia)
            {
                this.Focus();
            }
        }
        #endregion

        private void Button_Click(object sender, RoutedEventArgs e)
        {

            //LoadingBar.Visibility = System.Windows.Visibility.Visible;
            App.ProgressIndicator(true);
            if (App.NetworkAvailable)
            {
                if (UsuarioTextBox.Text.Length != 11)
                {
                    MessageBox.Show(AppResources.WrongUserText, AppResources.WrongUserTitle, MessageBoxButton.OK);
                    UsuarioTextBox.Focus();
                    //LoadingBar.Visibility = System.Windows.Visibility.Collapsed;
                    App.ProgressIndicator(false);
                }
                else if (_actualPassword.Length != 4)
                {
                    MessageBox.Show(AppResources.WrongPasswordText, AppResources.WrongPasswordTitle, MessageBoxButton.OK);
                    SenhaTextBox.Focus();
                    //LoadingBar.Visibility = System.Windows.Visibility.Collapsed;
                    App.ProgressIndicator(false);
                }
                else
                {
                    App.DataConexao.Usuario = UsuarioTextBox.Text;
                    App.DataConexao.Senha = _actualPassword;
                    App.DataConexao.LogarUsuario(TestarLoginCallback);
                }
            }
            else
            {
                App.ProgressIndicator(false);
                MessageBox.Show(AppResources.NetworkNotAvailableText, AppResources.NetworkNotAvailableTitle, MessageBoxButton.OK);                
            }
            
        }

        void TestarLoginCallback(IAsyncResult result)
        {
            Dispatcher.BeginInvoke(() =>
            {
                //LoadingBar.Visibility = System.Windows.Visibility.Collapsed;
                App.ProgressIndicator(false);
            });
            if (!App.DataConexao.Erro)
            {
                App.Usuario = App.DataConexao.Usuario;
                App.Senha = App.DataConexao.Senha;

                Dispatcher.BeginInvoke(() =>
                {
                    NavigationService.Navigate(new Uri("/LivrosPage.xaml", UriKind.Relative));
                });
            }
            else
            {
                if (App.DataConexao.SenhaIncorreta)
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show(AppResources.LoginErrorText, AppResources.LoginErrorTitle, MessageBoxButton.OK);
                    });
                }
                else
                {
                    Dispatcher.BeginInvoke(() =>
                    {
                        MessageBox.Show(AppResources.ConnectionErrorText, AppResources.ConnectionErrorTitle, MessageBoxButton.OK);
                    });
                }
                    
            }
        }

        // Sample code for building a localized ApplicationBar
        private void BuildLocalizedApplicationBar()
        {
            // Set the page's ApplicationBar to a new instance of ApplicationBar.
            ApplicationBar = new ApplicationBar();
            ApplicationBar.Mode = ApplicationBarMode.Minimized;

            // Create a new menu item with the localized string from AppResources.
            ApplicationBarMenuItem appBarAboutMenuItem = new ApplicationBarMenuItem(AppResources.AppBarAboutMenuItemText);
            appBarAboutMenuItem.Click += appBarAboutMenuItem_Click;
            ApplicationBar.MenuItems.Add(appBarAboutMenuItem);

        }

        private void appBarAboutMenuItem_Click(object sender, EventArgs e)
        {
            NavigationService.Navigate(new Uri("/AboutPage.xaml", UriKind.Relative));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            UsuarioTextBox.Text = "";
            SenhaTextBox.Text = "";
            _enteredPasscode = "";
            _actualPassword = "";
            contCaracteresUser = 0;
            contCaracteres = 0;
            bloqueia = false;
        }

    }
}