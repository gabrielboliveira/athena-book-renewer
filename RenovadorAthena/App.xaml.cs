﻿using System;
using System.Diagnostics;
using System.Resources;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using RenovadorAthena.Resources;
using RenovadorAthena.ViewModels;
using System.Text;
using System.Security.Cryptography;
using System.IO.IsolatedStorage;
using System.IO;
using Windows.Networking.Connectivity;
using System.Net.NetworkInformation;
using System.Collections.ObjectModel;
using System.Threading;

namespace RenovadorAthena
{
    public partial class App : Application
    {
        private static MainViewModel viewModel = null;

        /// <summary>
        /// A static ViewModel used by the views to bind against.
        /// </summary>
        /// <returns>The MainViewModel object.</returns>
        public static MainViewModel ViewModel
        {
            get
            {
                // Delay creation of the view model until necessary
                if (viewModel == null)
                    viewModel = new MainViewModel();

                return viewModel;
            }
        }

        /// <summary>
        /// Provides easy access to the root frame of the Phone Application.
        /// </summary>
        /// <returns>The root frame of the Phone Application.</returns>
        public static PhoneApplicationFrame RootFrame { get; private set; }

        private static AthenaData dataConexao = null;

        public static AthenaData DataConexao
        {
            get 
            {
                if (dataConexao == null)
                    dataConexao = new AthenaData();
                return dataConexao; 
            }
        }

        private static string usuario = null;

        public static string Usuario
        {
            get 
            { 
                if(usuario == null)
                    usuario = LoadEncryptedText(App.UsernameID);
                return usuario; 
            }
            set 
            { 
                usuario = value;
                SaveEncryptedText(usuario, App.UsernameID);
            }
        }

        private static string senha = null;

        public static string Senha
        {
            get
            {
                if (senha == null)
                    senha = LoadEncryptedText(App.PasswordID);
                return senha; 
            }
            set 
            { 
                senha = value;
                SaveEncryptedText(senha, App.PasswordID);
            }
        }

        public static string UsernameID = "Username";
        public static string PasswordID = "Password";
        public static string BooksID = "Books";

        private static string filePath = "EncryptedFile";

        private static void SaveEncryptedText(string textToEncrypt, string identifier)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            if (!settings.Contains(identifier))
            {
                settings.Add(identifier, true);
            }
            else
            {
                settings[identifier] = true;
            }
            settings.Save();

            // Convert the PIN to a byte[].
            byte[] PinByte = Encoding.UTF8.GetBytes(textToEncrypt);

            // Encrypt the PIN by using the Protect() method.
            byte[] ProtectedPinByte = ProtectedData.Protect(PinByte, null);

            // Create a file in the application's isolated storage.
            IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication();
            IsolatedStorageFileStream writestream = new IsolatedStorageFileStream(filePath + identifier, System.IO.FileMode.Create, System.IO.FileAccess.Write, file);

            // Write pinData to the file.
            Stream writer = new StreamWriter(writestream).BaseStream;
            writer.Write(ProtectedPinByte, 0, ProtectedPinByte.Length);
            writer.Close();
            writestream.Close();
        }

        private static string LoadEncryptedText(string identifier)
        {
            bool saved = false;
            if (IsolatedStorageSettings.ApplicationSettings.Contains(identifier))
            {
                saved = Convert.ToBoolean(IsolatedStorageSettings.ApplicationSettings[identifier]);
            }

            string text = null;

            IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication();

            if (saved && file.FileExists(filePath + identifier) )
            {
                // Access the file in the application's isolated storage.
                
                IsolatedStorageFileStream readstream = new IsolatedStorageFileStream(filePath + identifier, System.IO.FileMode.Open, FileAccess.Read, file);

                // Read the PIN from the file.
                Stream reader = new StreamReader(readstream).BaseStream;
                byte[] ProtectedPinByte = new byte[reader.Length];

                reader.Read(ProtectedPinByte, 0, ProtectedPinByte.Length);
                reader.Close();
                readstream.Close();

                // Decrypt the PIN by using the Unprotect method.
                byte[] PinByte = ProtectedData.Unprotect(ProtectedPinByte, null);

                // Convert the PIN from byte to string and display it in the text box.
                text = Encoding.UTF8.GetString(PinByte, 0, PinByte.Length);
            }

            return text;
        }

        private static void DeleteEncryptedText(string identifier)
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains(identifier))
            {
                settings.Remove(identifier);
            }
            settings.Save();

            IsolatedStorageFile file = IsolatedStorageFile.GetUserStoreForApplication();

            if (file.FileExists(filePath + identifier))
                file.DeleteFile(filePath + identifier);

            usuario = null;
            senha = null;
        }

        private static void SaveBooks()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            if (!settings.Contains(BooksID))
            {
                settings.Add(BooksID, App.ViewModel.Items);
            }
            else
            {
                settings[BooksID] = App.ViewModel.Items;
            }
            settings.Save();
        }

        private static void RemoveBooks()
        {
            IsolatedStorageSettings settings = IsolatedStorageSettings.ApplicationSettings;
            if (settings.Contains(BooksID))
            {
                settings.Remove(BooksID);
            }
            settings.Save();
            viewModel.IsDataLoaded = false;
        }

        public static void Logout()
        {
            DeleteEncryptedText(UsernameID);
            DeleteEncryptedText(PasswordID);
            RemoveBooks();
            dataConexao = null;
            App.ViewModel.Items.Clear();
        }

        private static bool networkAvailable = false;

        public static bool NetworkAvailable
        {
            get { return networkAvailable; }
            set { networkAvailable = value; }
        }

        /// <summary>
        /// Constructor for the Application object.
        /// </summary>
        public App()
        {
            // Global handler for uncaught exceptions.
            UnhandledException += Application_UnhandledException;

            // Standard XAML initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Language display initialization
            InitializeLanguage();

            // Show graphics profiling information while debugging.
            if (Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                Application.Current.Host.Settings.EnableFrameRateCounter = true;

                // Show the areas of the app that are being redrawn in each frame.
                //Application.Current.Host.Settings.EnableRedrawRegions = true;

                // Enable non-production analysis visualization mode,
                // which shows areas of a page that are handed off GPU with a colored overlay.
                //Application.Current.Host.Settings.EnableCacheVisualization = true;

                // Prevent the screen from turning off while under the debugger by disabling
                // the application's idle detection.
                // Caution:- Use this under debug mode only. Application that disables user idle detection will continue to run
                // and consume battery power when the user is not using the phone.
                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }

            NetworkInformation.NetworkStatusChanged += NetworkInformation_NetworkStatusChanged;
            networkAvailable = NetworkInterface.GetIsNetworkAvailable();
        }

        private void NetworkInformation_NetworkStatusChanged(object sender)
        {
            networkAvailable = NetworkInterface.GetIsNetworkAvailable();
        }

        public static void ProgressIndicator(bool show)
        {
            if (SystemTray.ProgressIndicator == null)
            {
                SystemTray.ProgressIndicator = new ProgressIndicator();
            }
            SystemTray.ProgressIndicator.IsIndeterminate = true;
            SystemTray.ProgressIndicator.IsVisible = show;
        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
            //Thread.Sleep(1000);
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
            //// Ensure that application state is restored appropriately
            //if (!App.ViewModel.IsDataLoaded)
            //{
            //    LoadBooks();
            //}
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
            // Ensure that required application state is persisted here.
            SaveBooks();
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {
            SaveBooks();
        }

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                Debugger.Break();
            }
        }

        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Handle reset requests for clearing the backstack
            RootFrame.Navigated += CheckForResetNavigation;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }

        private void CheckForResetNavigation(object sender, NavigationEventArgs e)
        {
            // If the app has received a 'reset' navigation, then we need to check
            // on the next navigation to see if the page stack should be reset
            if (e.NavigationMode == NavigationMode.Reset)
                RootFrame.Navigated += ClearBackStackAfterReset;
        }

        private void ClearBackStackAfterReset(object sender, NavigationEventArgs e)
        {
            // Unregister the event so it doesn't get called again
            RootFrame.Navigated -= ClearBackStackAfterReset;

            // Only clear the stack for 'new' (forward) and 'refresh' navigations
            if (e.NavigationMode != NavigationMode.New && e.NavigationMode != NavigationMode.Refresh)
                return;

            // For UI consistency, clear the entire page stack
            while (RootFrame.RemoveBackEntry() != null)
            {
                ; // do nothing
            }
        }

        #endregion

        // Initialize the app's font and flow direction as defined in its localized resource strings.
        //
        // To ensure that the font of your application is aligned with its supported languages and that the
        // FlowDirection for each of those languages follows its traditional direction, ResourceLanguage
        // and ResourceFlowDirection should be initialized in each resx file to match these values with that
        // file's culture. For example:
        //
        // AppResources.es-ES.resx
        //    ResourceLanguage's value should be "es-ES"
        //    ResourceFlowDirection's value should be "LeftToRight"
        //
        // AppResources.ar-SA.resx
        //     ResourceLanguage's value should be "ar-SA"
        //     ResourceFlowDirection's value should be "RightToLeft"
        //
        // For more info on localizing Windows Phone apps see http://go.microsoft.com/fwlink/?LinkId=262072.
        //
        private void InitializeLanguage()
        {
            try
            {
                // Set the font to match the display language defined by the
                // ResourceLanguage resource string for each supported language.
                //
                // Fall back to the font of the neutral language if the Display
                // language of the phone is not supported.
                //
                // If a compiler error is hit then ResourceLanguage is missing from
                // the resource file.
                RootFrame.Language = XmlLanguage.GetLanguage(AppResources.ResourceLanguage);

                // Set the FlowDirection of all elements under the root frame based
                // on the ResourceFlowDirection resource string for each
                // supported language.
                //
                // If a compiler error is hit then ResourceFlowDirection is missing from
                // the resource file.
                FlowDirection flow = (FlowDirection)Enum.Parse(typeof(FlowDirection), AppResources.ResourceFlowDirection);
                RootFrame.FlowDirection = flow;
            }
            catch
            {
                // If an exception is caught here it is most likely due to either
                // ResourceLangauge not being correctly set to a supported language
                // code or ResourceFlowDirection is set to a value other than LeftToRight
                // or RightToLeft.

                if (Debugger.IsAttached)
                {
                    Debugger.Break();
                }

                throw;
            }
        }
    }
}